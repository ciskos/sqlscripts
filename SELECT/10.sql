SELECT
      fiz_2006_ne_null.pr_numident,
      fiz_2006_ne_null.pr_name,
      SUM(fiz_2006_ne_null.sum_1),
      SUM(fiz_2006_ne_null.sum_2),
      SUM(fiz_2006_ne_null.sum_3),
      SUM(fiz_2006_ne_null.sum_4),
      SUM(fiz_2006_ne_null.sum_5),
      SUM(fiz_2006_ne_null.sum_6),
      SUM(fiz_2006_ne_null.sum_7),
      SUM(fiz_2006_ne_null.sum_8),
      SUM(fiz_2006_ne_null.sum_9),
      SUM(fiz_2006_ne_null.sum_10),
      SUM(fiz_2006_ne_null.sum_11),
      SUM(fiz_2006_ne_null.sum_12),
      SUM(fiz_2006_ne_null.sum_13),
      SUM(fiz_2006_ne_null.sum_14),
      SUM(fiz_2006_ne_null.sum_15),
      SUM(fiz_2006_ne_null.sum_16),
      SUM(fiz_2006_ne_null.sum_17),
      SUM(fiz_2006_ne_null.sum_18),
      COUNT(fiz_2006_ne_null.pr_name),
      fiz_2006_ne_null.insp_doh,
      users.usr_fio
FROM
      fiz_2006_ne_null AS fiz_2006_ne_null LEFT JOIN payer AS payer
      ON fiz_2006_ne_null.pr_numident=payer.pr_numident
      LEFT JOIN users AS users ON payer.pr_oper_numid=users.usr_extnum
GROUP BY
      fiz_2006_ne_null.pr_name
ORDER BY
      users.usr_fio,
      fiz_2006_ne_null.pr_numident,
      fiz_2006_ne_null.month
