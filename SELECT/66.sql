SELECT
	perssheets_10r.uind_numident AS Код_ЗО,
	packlabel_10r.lb_id_in as номер_пачки,
	indani_pib_10r.uip_ln AS Фамилия,
	indani_pib_10r.uip_nm AS Имя,
	indani_pib_10r.uip_ftn AS Отчество,
	payer_10r.pr_numident AS Код_РД,
	payer_10r.pr_incode AS Филиал,
	payer_10r.pr_name1 AS Полное_название_РД,
	perssheets_10r.uind_charg_year AS Год_начисления,
	(CASE perssheets_10r.uind_formtype WHEN '1' THEN 'Поч'
		WHEN '4' THEN 'Пенс'
		WHEN '2' THEN 'Кор' END),
	(CASE perssheets_10r.uind_status WHEN 'N' THEN 'Редагується'
		WHEN 'G' THEN 'Без помилок'
		WHEN 'E' THEN 'Помилковий'
		WHEN 'R' THEN 'До в_дправки'
		WHEN 'S' THEN 'В_дправлений'
		WHEN 'X' THEN 'В_дхилений Ц'
		WHEN 'O' THEN 'Оброблений Ц' END),
	perssheets_10r.uind_otk  AS Трудовая_книжка,
	perssheets_10r.uind_summp AS ЗП_усього,
	perssheets_10r.uind_summl AS Сума_лiк,
	perssheets_10r.uind_summpc AS ЗП_врахов,
        perssheets_10r.uind_summ_pr AS Сума_внескiв_iз_РД,
	perssheets_10r.uind_summ_zo AS Сума_збор_ЗО
FROM
	perssheets_10r AS perssheets_10r LEFT JOIN indani_pib_10r AS indani_pib_10r
	ON indani_pib_10r.uip_uind=perssheets_10r.uind_id
	LEFT JOIN packlabel_10r AS packlabel_10r ON packlabel_10r.lb_id=perssheets_10r.uind_lb
	LEFT JOIN payer_10r AS payer_10r ON payer_10r.pr_id=packlabel_10r.lb_pr



packlabel,payer
where
 (PR_ID=LB_PR)
AND (LB_ID=UIND_LB)
AND (UIND_ID=UIP_UIND)
AND (uind_status IN ('G','E','R','X','Y','N'))
AND (YEAR(uind_charg_year) IN (2000,2001,2002,2003,2004,2005,2006,2007,2008,2009))
 and ((packlabel.lock_flag<>"D") or
 (packlabel.lock_flag is null)) and ((payer.lock_flag<>"D") or
 (payer.lock_flag is null));
