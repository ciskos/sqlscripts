SELECT
      svidet_deadmen.*,
      indani_pib.uip_ln,
      indani_pib.uip_nm,
      indani_pib.uip_ftn,
      svidet_spov.*
FROM
      svidet_spov As svidet_spov JOIN svidet_deadmen AS svidet_deadmen
      ON svidet_spov.id=svidet_deadmen.uind_numident
      LEFT JOIN perssheets AS perssheets
      ON perssheets.uind_numident=svidet_deadmen.uind_numident
      LEFT JOIN indani_pib AS indani_pib
      ON indani_pib.uip_uind=perssheets.uind_id
GROUP BY
      svidet_deadmen.uind_numident
