SELECT
      ers.pr_name,
      ers.pr_numident,
      payer.pr_name1,
      payer.pr_numident,
      "ers"
FROM
      ers AS ers LEFT JOIN payer AS payer ON ers.pr_numident=payer.pr_numident
WHERE
      payer.pr_numident IS NULL

UNION

SELECT
      payer.pr_name1,
      payer.pr_numident,
      ers.pr_name,
      ers.pr_numident,
      "spov"
FROM
      payer AS payer LEFT JOIN ers AS ers ON payer.pr_numident=ers.pr_numident
WHERE
      ers.pr_numident IS NULL
