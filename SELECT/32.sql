SELECT
	diff_spov_2006.pr_numident,
	payer_malinv41r.pr_numident,
	payer_malinv41r.pr_name1,
	diff_spov_2006.uind_numident,
	perssheets_malinv41r.uind_numident,
	indani_pib_malinv41r.uip_ln,
	indani_pib_malinv41r.uip_nm,
	indani_pib_malinv41r.uip_ftn,
	users_malinv41r.usr_fio
FROM
	perssheets_malinv41r AS perssheets_malinv41r
	LEFT JOIN diff_spov_2006 AS diff_spov_2006
	ON perssheets_malinv41r.uind_numident=diff_spov_2006.uind_numident


	LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=diff_spov_2006.pr_numident

	LEFT JOIN indani_pib_malinv41r AS indani_pib_malinv41r
	ON indani_pib_malinv41r.uip_uind=perssheets_malinv41r.uind_id

	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	diff_spov_2006.uind_numident,
	diff_spov_2006.pr_numident
ORDER BY
	users_malinv41r.usr_fio,
	diff_spov_2006.pr_numident,
	diff_spov_2006.uind_numident
