SELECT
	diff_spov_2006.*,
	sv.tin,
	sv.fulln_u,
	sv.name_u,
	sv.fath_u
FROM
	diff_spov_2006 AS diff_spov_2006 LEFT JOIN sv AS sv
	ON sv.tin=diff_spov_2006.uind_numident
WHERE
	sv.tin IS NOT NULL
