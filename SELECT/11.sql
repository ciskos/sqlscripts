SELECT
      ur_2006_ne_null.pr_numident,
      ur_2006_ne_null.pr_name,
      SUM(ur_2006_ne_null.sum_1),
      SUM(ur_2006_ne_null.sum_2),
      SUM(ur_2006_ne_null.sum_3),
      SUM(ur_2006_ne_null.sum_4),
      SUM(ur_2006_ne_null.sum_5),
      SUM(ur_2006_ne_null.sum_6),
      SUM(ur_2006_ne_null.sum_7),
      SUM(ur_2006_ne_null.sum_8),
      SUM(ur_2006_ne_null.sum_9),
      SUM(ur_2006_ne_null.sum_10),
      SUM(ur_2006_ne_null.sum_11),
      SUM(ur_2006_ne_null.sum_12),
      SUM(ur_2006_ne_null.sum_13),
      SUM(ur_2006_ne_null.sum_14),
      SUM(ur_2006_ne_null.sum_15),
      SUM(ur_2006_ne_null.sum_16),
      SUM(ur_2006_ne_null.sum_17),
      SUM(ur_2006_ne_null.sum_18),
      COUNT(ur_2006_ne_null.pr_name),
      ur_2006_ne_null.insp_doh,
      users.usr_fio
FROM
      ur_2006_ne_null AS ur_2006_ne_null LEFT JOIN payer AS payer
      ON ur_2006_ne_null.pr_numident=payer.pr_numident
      LEFT JOIN users AS users ON payer.pr_oper_numid=users.usr_extnum
GROUP BY
      ur_2006_ne_null.pr_name
ORDER BY
      users.usr_fio,
      ur_2006_ne_null.pr_numident,
      ur_2006_ne_null.month
