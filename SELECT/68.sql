SELECT
	COUNT(unload_3_10r.lb_id_in)
FROM
	unload_3_10r LEFT JOIN perssheets_10r
	ON unload_3_10r.uind_numident=perssheets_10r.uind_numident
	LEFT JOIN packlabel_10r
	ON packlabel_10r.lb_id=perssheets_10r.uind_lb
GROUP BY
	unload_3_10r.lb_id_in;
