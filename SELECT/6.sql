SELECT
      empty_2005.pr_numident,
      empty_2005.pr_name,
      payer.pr_numident,
      payer.pr_name1,
      users.usr_fio
FROM
      empty_2005 AS empty_2005
      LEFT JOIN payer AS payer
      ON empty_2005.pr_numident=payer.pr_numident
      LEFT JOIN users AS users ON payer.pr_oper_numid=users.usr_extnum
ORDER BY
      users.usr_fio,
      empty_2005.pr_numident
