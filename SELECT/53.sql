SELECT
	  4pf,
      us_ur.pr_numident,
      us_ur.pr_name1,
	  COUNT(us_ur.pr_name1),
      SUM(us_ur.sum1),
      SUM(us_ur.sum2),
      SUM(us_ur.sum3),
      SUM(us_ur.sum4),
      SUM(us_ur.sum5),
      SUM(us_ur.sum6),
      SUM(us_ur.sum7),
      SUM(us_ur.sum8),
      SUM(us_ur.sum9),
      SUM(us_ur.sum10),
      SUM(us_ur.sum11),
      SUM(us_ur.sum12),
      SUM(us_ur.sum13),
      SUM(us_ur.sum14),
      SUM(us_ur.sum15),
      SUM(us_ur.sum16),
      SUM(us_ur.sum17),
      SUM(us_ur.sum18),
      SUM(us_ur.sum19),
      SUM(us_ur.sum20),
      SUM(us_ur.sum21),
      SUM(us_ur.sum22),
      SUM(us_ur.sum23),
      users_malinv41r.usr_fio
FROM
      us_ur AS us_ur LEFT JOIN payer_malinv41r AS payer_malinv41r
      ON us_ur.pr_numident=payer_malinv41r.pr_numident
      LEFT JOIN users_malinv41r AS users_malinv41r ON payer_malinv41r.pr_oper_numid=users_malinv41r.usr_extnum
GROUP BY
      4pf
ORDER BY
      users_malinv41r.usr_fio,
      us_ur.pr_numident
