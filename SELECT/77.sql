SELECT
*
FROM
	unload_3_10r AS unload_3_10r LEFT JOIN payer_10r AS payer_10r
	ON unload_3_10r.pr_numident=payer_10r.pr_numident
	AND unload_3_10r.pr_incode=payer_10r.pr_incode
	LEFT JOIN perssheets_10r AS perssheets_10r
	ON unload_3_10r.uind_numident=perssheets_10r.uind_numident
	AND unload_3_10r.uind_charg_year=perssheets_10r.uind_charg_year
	AND unload_3_10r.uind_formtype=perssheets_10r.uind_formtype
	LEFT JOIN packlabel_10r AS packlabel_10r
	ON packlabel_10r.lb_id=perssheets_10r.uind_lb
	AND packlabel_10r.lb_pr=payer_10r.pr_id
	LEFT JOIN ikis2spov_users AS ikis2spov_users
	ON ikis2spov_users.pr_numident=unload_3_10r.pr_numident
	LEFT JOIN users_ikis AS users_ikis
	ON users_ikis.usr_extnum=ikis2spov_users.usr_extnum
WHERE
	payer_10r.pr_numident IS NULL
	AND payer_10r.pr_incode IS NULL
	AND perssheets_10r.uind_numident IS NULL
	AND perssheets_10r.uind_formtype IS NULL
	AND perssheets_10r.uind_charg_year IS NULL
ORDER BY
	users_ikis.usr_fio
	INTO OUTFILE '/home/kot/work/table/upload/outload_10r_raznica_full' fields terminated by '|';
