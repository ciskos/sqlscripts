SELECT
      org_ved_new.pr_numident,
      org_ved_new.pr_filial,
      org_ved_new.pr_name1,
      svidet_found.rajon,
      svidet_found.pr_numident,
      svidet_found.uind_numident
FROM
      org_ved_new AS org_ved_new,
      svidet_found AS svidet_found
WHERE
      org_ved_new.pr_numident=svidet_found.pr_numident
      AND svidet_found.rajon='1541'
ORDER BY
      org_ved_new.pr_numident
LIMIT 0,30
