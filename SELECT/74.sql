SELECT
	unload_3_41r.pr_numident,
	ikis_indani.pr_numident,
	unload_3_41r.pr_incode,
	ikis_indani.pr_incode,
	unload_3_41r.uind_numident,
	ikis_indani.uind_numident,
	unload_3_41r.uind_charg_year,
	ikis_indani.uind_charg_year,
	unload_3_41r.uind_formtype,
	ikis_indani.uind_formtype,
	unload_3_41r.pr_name1,
	ikis_indani.pr_name1
FROM
	unload_3_41r AS unload_3_41r LEFT JOIN ikis_indani AS ikis_indani
	ON unload_3_41r.pr_numident=ikis_indani.pr_numident
	AND unload_3_41r.pr_incode=ikis_indani.pr_incode
	AND unload_3_41r.uind_numident=ikis_indani.uind_numident
	AND unload_3_41r.uind_charg_year=ikis_indani.uind_charg_year
	AND unload_3_41r.uind_formtype=ikis_indani.uind_formtype
WHERE
	ikis_indani.pr_numident IS NULL
	AND unload_3_41r.uind_numident='0937800679'
	AND unload_3_41r.pr_numident='00205848'
