SELECT
diff_spov_2006.*,
COUNT(diff_spov_2006.pr_numident),
users_malinv41r.usr_fio
FROM
diff_spov_2006 AS diff_spov_2006 LEFT JOIN payer_malinv41r AS payer_malinv41r
ON payer_malinv41r.pr_numident=diff_spov_2006.pr_numident
LEFT JOIN users_malinv41r AS users_malinv41r
ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
diff_spov_2006.pr_numident
ORDER BY
users_malinv41r.usr_fio,
COUNT(diff_spov_2006.pr_numident)
