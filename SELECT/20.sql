SELECT
      fiz_2006_070405.*,
      users.usr_fio
FROM
      fiz_2006_070405 AS fiz_2006_070405 LEFT JOIN payer AS payer
      ON fiz_2006_070405.pr_numident=payer.pr_numident
      LEFT JOIN users AS users ON payer.pr_oper_numid=users.usr_extnum
ORDER BY
      users.usr_fio,
      fiz_2006_070405.pr_numident,
      fiz_2006_070405.month
