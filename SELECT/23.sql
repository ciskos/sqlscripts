SELECT
      COUNT(ur_2006_070405.pr_name),
      ur_2006_070405.pr_numident,
      ur_2006_070405.pr_name,
      ur_2006_070405.pr_cod_psv,
      SUM(ur_2006_070405.sum_1),
      SUM(ur_2006_070405.sum_2),
      SUM(ur_2006_070405.sum_3),
      SUM(ur_2006_070405.sum_4),
      SUM(ur_2006_070405.sum_5),
      SUM(ur_2006_070405.sum_6),
      SUM(ur_2006_070405.sum_7),
      SUM(ur_2006_070405.sum_8),
      SUM(ur_2006_070405.sum_9),
      SUM(ur_2006_070405.sum_10),
      SUM(ur_2006_070405.sum_11),
      SUM(ur_2006_070405.sum_12),
      SUM(ur_2006_070405.sum_13),
      SUM(ur_2006_070405.sum_14),
      SUM(ur_2006_070405.sum_15),
      SUM(ur_2006_070405.sum_16),
      ur_2006_070405.doh_insp,
      users.usr_fio
FROM
      ur_2006_070405 AS ur_2006_070405 LEFT JOIN payer AS payer
      ON ur_2006_070405.pr_numident=payer.pr_numident
      LEFT JOIN users AS users ON payer.pr_oper_numid=users.usr_extnum
GROUP BY
      ur_2006_070405.pr_cod_psv
ORDER BY
      users.usr_fio,
      ur_2006_070405.pr_numident,
      ur_2006_070405.month
