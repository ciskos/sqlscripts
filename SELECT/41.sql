SELECT
	4pf,
	2008fiz.pr_numident,
	2008fiz.pr_name1,
	2008fiz.pens_insp,
	MAX(2008fiz.month),
	MAX(2008fiz.max1),
	SUM(2008fiz.sum1),
	SUM(2008fiz.sum2),
	SUM(2008fiz.sum3),
	SUM(2008fiz.sum4),
	SUM(2008fiz.sum5),
	SUM(2008fiz.sum6),
	SUM(2008fiz.sum7),
	SUM(2008fiz.sum8),
	SUM(2008fiz.sum9),
	SUM(2008fiz.sum10),
	SUM(2008fiz.sum11),
	SUM(2008fiz.sum12),
	SUM(2008fiz.sum13),
	SUM(2008fiz.sum14),
	SUM(2008fiz.sum15),
	SUM(2008fiz.sum16),
	SUM(2008fiz.sum17),
	SUM(2008fiz.sum18),
	SUM(2008fiz.sum19),
	MAX(2008fiz.month_2),
	SUM(2008fiz.sum20),
	SUM(2008fiz.sum21),
	SUM(2008fiz.sum22),
	users_malinv41r.usr_fio
FROM
	2008fiz AS 2008fiz LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=2008fiz.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	2008fiz.pr_numident
ORDER BY
	users_malinv41r.usr_fio,
	2008fiz.pr_numident
