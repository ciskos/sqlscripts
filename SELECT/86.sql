SELECT
	ur_2010.pf_num,
	ur_2010.pr_numident,
	ur_2010.pr_name,
	ur_2010.doh_insp,
	MAX(ur_2010.months),
	SUM(ur_2010.sum_1),
	SUM(ur_2010.sum_2),
	SUM(ur_2010.sum_3),
	SUM(ur_2010.sum_4),
	SUM(ur_2010.sum_5),
	SUM(ur_2010.sum_6),
	SUM(ur_2010.sum_7),
	SUM(ur_2010.sum_8),
	SUM(ur_2010.sum_9),
	SUM(ur_2010.sum_10),
	SUM(ur_2010.sum_11),
	SUM(ur_2010.sum_12),
	SUM(ur_2010.sum_13),
	SUM(ur_2010.sum_14),
	SUM(ur_2010.sum_15),
	SUM(ur_2010.sum_16),
	SUM(ur_2010.sum_17),
	SUM(ur_2010.sum_18),
	SUM(ur_2010.sum_19),
	SUM(ur_2010.sum_20),
	SUM(ur_2010.sum_21),
	SUM(ur_2010.sum_22),
	users_ikis_spov.usr_fio
FROM
	ur_2010 AS ur_2010 LEFT JOIN users_ikis_spov AS users_ikis_spov
	ON users_ikis_spov.pr_numident=ur_2010.pr_numident
GROUP BY
	ur_2010.pr_numident
ORDER BY
	users_ikis_spov.usr_fio,
	ur_2010.pr_numident
INTO OUTFILE '/home/kot/tmp/summ/ur2010_utf8.csv'
