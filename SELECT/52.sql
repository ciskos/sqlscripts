SELECT
	  4pf,
      us_fiz.pr_numident,
      us_fiz.pr_name1,
	  COUNT(us_fiz.pr_name1),
      SUM(us_fiz.sum1),
      SUM(us_fiz.sum2),
      SUM(us_fiz.sum3),
      SUM(us_fiz.sum4),
      SUM(us_fiz.sum5),
      SUM(us_fiz.sum6),
      SUM(us_fiz.sum7),
      SUM(us_fiz.sum8),
      SUM(us_fiz.sum9),
      SUM(us_fiz.sum10),
      SUM(us_fiz.sum11),
      SUM(us_fiz.sum12),
      SUM(us_fiz.sum13),
      SUM(us_fiz.sum14),
      SUM(us_fiz.sum15),
      SUM(us_fiz.sum16),
      SUM(us_fiz.sum17),
      SUM(us_fiz.sum18),
      SUM(us_fiz.sum19),
      SUM(us_fiz.sum20),
      SUM(us_fiz.sum21),
      SUM(us_fiz.sum22),
      SUM(us_fiz.sum23),
      users_malinv41r.usr_fio
FROM
      us_fiz AS us_fiz LEFT JOIN payer_malinv41r AS payer_malinv41r
      ON us_fiz.pr_numident=payer_malinv41r.pr_numident
      LEFT JOIN users_malinv41r AS users_malinv41r ON payer_malinv41r.pr_oper_numid=users_malinv41r.usr_extnum
GROUP BY
      4pf
ORDER BY
      users_malinv41r.usr_fio,
      us_fiz.pr_numident
