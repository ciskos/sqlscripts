SELECT
	db_insp_spisok.krpou,
	db_insp_spisok.tin,
	payer_malinv41r.pr_numident,
	payer_malinv41r.pr_name1,
	users_malinv41r.usr_fio
FROM
	db_insp_spisok AS db_insp_spisok LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=db_insp_spisok.krpou
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
LIMIT 0,10;
