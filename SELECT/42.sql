SELECT
	4pf,
	2008ur.pr_numident,
	2008ur.pr_name1,
	2008ur.pens_insp,
	MAX(2008ur.month),
	MAX(2008ur.max1),
	SUM(2008ur.sum1),
	SUM(2008ur.sum2),
	SUM(2008ur.sum3),
	SUM(2008ur.sum4),
	SUM(2008ur.sum5),
	SUM(2008ur.sum6),
	SUM(2008ur.sum7),
	SUM(2008ur.sum8),
	SUM(2008ur.sum9),
	SUM(2008ur.sum10),
	SUM(2008ur.sum11),
	SUM(2008ur.sum12),
	SUM(2008ur.sum13),
	SUM(2008ur.sum14),
	SUM(2008ur.sum15),
	SUM(2008ur.sum16),
	SUM(2008ur.sum17),
	SUM(2008ur.sum18),
	SUM(2008ur.sum19),
	users_malinv41r.usr_fio
FROM
	2008ur AS 2008ur LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=2008ur.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	2008ur.pr_numident
ORDER BY
	users_malinv41r.usr_fio,
	2008ur.pr_numident
