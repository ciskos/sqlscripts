SELECT
      COUNT(fiz_2006_070405.pr_name),
      fiz_2006_070405.pr_numident,
      fiz_2006_070405.pr_name,
      fiz_2006_070405.pr_cod_psv,
      SUM(fiz_2006_070405.sum_1),
      SUM(fiz_2006_070405.sum_2),
      SUM(fiz_2006_070405.sum_3),
      SUM(fiz_2006_070405.sum_4),
      SUM(fiz_2006_070405.sum_5),
      SUM(fiz_2006_070405.sum_6),
      SUM(fiz_2006_070405.sum_7),
      SUM(fiz_2006_070405.sum_8),
      SUM(fiz_2006_070405.sum_9),
      SUM(fiz_2006_070405.sum_10),
      SUM(fiz_2006_070405.sum_11),
      SUM(fiz_2006_070405.sum_12),
      SUM(fiz_2006_070405.sum_13),
      SUM(fiz_2006_070405.sum_14),
      SUM(fiz_2006_070405.sum_15),
      SUM(fiz_2006_070405.sum_16),
      fiz_2006_070405.doh_insp,
      users.usr_fio
FROM
      fiz_2006_070405 AS fiz_2006_070405 LEFT JOIN payer AS payer
      ON fiz_2006_070405.pr_numident=payer.pr_numident
      LEFT JOIN users AS users ON payer.pr_oper_numid=users.usr_extnum
GROUP BY
      fiz_2006_070405.pr_cod_psv
ORDER BY
      users.usr_fio,
      fiz_2006_070405.pr_numident,
      fiz_2006_070405.month
