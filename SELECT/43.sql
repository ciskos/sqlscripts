SELECT
    payer_malinv41r.pr_numident,
	payer_ank_malinv41r.pr_cod_psv,
    payer_malinv41r.pr_name1,
    payer_ank_malinv41r.pr_name2,
    payer_ank_malinv41r.pr_name3,
    users_malinv41r.usr_fio
FROM
    payer_malinv41r AS payer_malinv41r LEFT JOIN payer_ank_malinv41r AS payer_ank_malinv41r
    ON payer_ank_malinv41r.pr_id=payer_malinv41r.pr_id
    LEFT JOIN users_malinv41r AS users_malinv41r ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
WHERE
    payer_malinv41r.pr_payercat BETWEEN 9 AND 26
ORDER BY
    users_malinv41r.usr_fio,
    payer_malinv41r.pr_numident
