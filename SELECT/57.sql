SELECT
	fiz_sum_2009.id,
	fiz_sum_2009.pf_code,
	fiz_sum_2009.pr_numident,
	COUNT(	fiz_sum_2009.pr_numident),
	fiz_sum_2009.pr_name1,
	fiz_sum_2009.pens_insp,
	SUM(fiz_sum_2009.sum1),
	SUM(fiz_sum_2009.sum2),
	SUM(fiz_sum_2009.sum3),
	SUM(fiz_sum_2009.sum4),
	SUM(fiz_sum_2009.sum5),
	SUM(fiz_sum_2009.sum6),
	SUM(fiz_sum_2009.sum7),
	SUM(fiz_sum_2009.sum8),
	SUM(fiz_sum_2009.sum9),
	SUM(fiz_sum_2009.sum10),
	SUM(fiz_sum_2009.sum11),
	SUM(fiz_sum_2009.sum12),
	SUM(fiz_sum_2009.sum13),
	SUM(fiz_sum_2009.sum14),
	SUM(fiz_sum_2009.sum15),
	SUM(fiz_sum_2009.sum16),
	SUM(fiz_sum_2009.sum17),
	SUM(fiz_sum_2009.sum18),
	SUM(fiz_sum_2009.sum19),
	SUM(fiz_sum_2009.sum20),
	SUM(fiz_sum_2009.sum21),
	SUM(fiz_sum_2009.sum22),
	SUM(fiz_sum_2009.sum23),
	SUM(fiz_sum_2009.sum24),
	SUM(fiz_sum_2009.sum25),
	SUM(fiz_sum_2009.sum26),
	SUM(fiz_sum_2009.sum27),
	SUM(fiz_sum_2009.sum28),
	SUM(fiz_sum_2009.sum29),
	users_41r.usr_fio
FROM
	fiz_sum_2009 AS fiz_sum_2009 LEFT JOIN payer_41r AS payer_41r
	ON payer_41r.pr_numident=fiz_sum_2009.pr_numident
	LEFT JOIN users_41r AS users_41r
	ON users_41r.usr_extnum=payer_41r.pr_oper_numid
GROUP BY
	fiz_sum_2009.pr_numident
ORDER BY
	users_41r.usr_fio
INTO OUTFILE
	'/home/kot/work/table/upload/fiz_sum_2009_out' FIELDS TERMINATED BY '|';
