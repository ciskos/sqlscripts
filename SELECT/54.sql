SELECT
	ikis_spov_payers_insps.*,
	users_malinv41r.usr_fio,
	payer_malinv41r.pr_numident,
	payer_malinv41r.pr_incode,
	payer_malinv41r.pr_name1
FROM
	ikis_spov_payers_insps AS ikis_spov_payers_insps LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON ikis_spov_payers_insps.pr_numident=payer_malinv41r.pr_numident
	AND ikis_spov_payers_insps.pr_incode=payer_malinv41r.pr_incode
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON payer_malinv41r.pr_oper_numid=users_malinv41r.usr_extnum
ORDER BY
	users_malinv41r.usr_fio
