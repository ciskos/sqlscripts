SELECT
	unload_3_10r.uind_numident,
	perssheets_10r.uind_numident,
	unload_3_10r.lb_id_in,
	packlabel_10r.lb_id_in,
	unload_3_10r.uip_ln,
	unload_3_10r.uip_nm,
	unload_3_10r.uip_ftn,
	unload_3_10r.pr_numident,
	payer_10r.pr_numident,
	unload_3_10r.pr_incode,
	payer_10r.pr_incode,
	unload_3_10r.pr_name1,
	payer_10r.pr_name1,
	unload_3_10r.uind_charg_year,
	perssheets_10r.uind_charg_year,
	unload_3_10r.uind_formtype,
	perssheets_10r.uind_formtype,
	unload_3_10r.uind_status,
	unload_3_10r.uind_otk,
	unload_3_10r.uind_summp,
	unload_3_10r.uind_summl,
	unload_3_10r.uind_summpc,
	unload_3_10r.uind_summ_pr,
	unload_3_10r.uind_summ_zo
FROM
	unload_3_10r RIGHT JOIN perssheets_10r
	ON unload_3_10r.uind_numident=perssheets_10r.uind_numident
	LEFT JOIN packlabel_10r ON packlabel_10r.lb_id=perssheets_10r.uind_lb
	LEFT JOIN payer_10r ON payer_10r.pr_id=packlabel_10r.lb_pr
WHERE
	unload_3_10r.uind_numident<>perssheets_10r.uind_numident
	AND unload_3_10r.lb_id_in<>packlabel_10r.lb_id_in limit 0,1;
