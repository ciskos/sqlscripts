SELECT
	ur2008.*,
	users_malinv41r.usr_fio
FROM
	ur2008 AS ur2008 LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON ur2008.pr_numident=payer_malinv41r.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
ORDER BY
	users_malinv41r.usr_fio
