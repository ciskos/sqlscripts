SELECT
	raznie_codes.*,
	payer_malinv41r.pr_name1,
	users_malinv41r.usr_fio
FROM
	raznie_codes AS raznie_codes LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=raznie_codes.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
	AND payer_malinv41r.pr_incode IS NULL
ORDER BY
	users_malinv41r.usr_fio,
	raznie_codes.pr_numident
