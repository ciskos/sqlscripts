SELECT
      payer.pr_numident,
      payer.pr_name1,
      payer_ank.pr_name2,
      payer_ank.pr_name3,
      users.usr_fio
FROM
      payer AS payer LEFT JOIN payer_ank AS payer_ank
      ON payer_ank.pr_id=payer.pr_id
      LEFT JOIN users AS users ON users.usr_extnum=payer.pr_oper_numid
WHERE
      payer.pr_payercat BETWEEN 9 AND 26
ORDER BY
      users.usr_fio,
      payer.pr_numident
