SELECT
	unload_3_10r.uind_numident,
	unload_3_10r.lb_id_in,
	unload_3_10r.uip_ln,
	unload_3_10r.uip_nm,
	unload_3_10r.uip_ftn,
	unload_3_10r.pr_numident,
	unload_3_10r.pr_incode,
	unload_3_10r.pr_name1,
	unload_3_10r.uind_charg_year,
	(CASE unload_3_10r.uind_formtype
		WHEN '1' THEN 'Початкова'
		WHEN '2' THEN 'Коригуюча'
		WHEN '3' THEN 'Скасовуюча'
		WHEN '4' THEN 'Призначення пенсии'
	END),
	unload_3_10r.uind_status,
	unload_3_10r.uind_otk,
	unload_3_10r.uind_summp,
	unload_3_10r.uind_summl,
	unload_3_10r.uind_summpc,
	unload_3_10r.uind_summ_pr,
	unload_3_10r.uind_summ_zo,
	users_ikis.usr_fio
FROM
	unload_3_10r AS unload_3_10r LEFT JOIN payer_10r AS payer_10r
	ON unload_3_10r.pr_numident=payer_10r.pr_numident
	AND unload_3_10r.pr_incode=payer_10r.pr_incode
	LEFT JOIN perssheets_10r AS perssheets_10r
	ON unload_3_10r.uind_numident=perssheets_10r.uind_numident
	AND unload_3_10r.uind_charg_year=perssheets_10r.uind_charg_year
	AND unload_3_10r.uind_formtype=perssheets_10r.uind_formtype
	LEFT JOIN packlabel_10r AS packlabel_10r
	ON packlabel_10r.lb_id=perssheets_10r.uind_lb
	AND packlabel_10r.lb_pr=payer_10r.pr_id
	LEFT JOIN ikis2spov_users AS ikis2spov_users
	ON ikis2spov_users.pr_numident=unload_3_10r.pr_numident
	LEFT JOIN users_ikis AS users_ikis
	ON users_ikis.usr_extnum=ikis2spov_users.usr_extnum
WHERE
	payer_10r.pr_numident IS NULL
ORDER BY
	users_ikis.usr_fio
	INTO OUTFILE '/home/kot/work/table/upload/outload_10r_raznica' fields terminated by '|';
