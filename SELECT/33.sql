SELECT
	diff_spov_2006.*,
	sv.tin,
	sv.fulln_u,
	sv.name_u,
	sv.fath_u
FROM
	sv AS sv LEFT JOIN diff_spov_2006 AS diff_spov_2006
	ON sv.tin=diff_spov_2006.uind_numident
WHERE
	diff_spov_2006.uind_numident IS NOT NULL
