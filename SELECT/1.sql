SELECT
      dodato23_4pf_group_by.pr_cod_psv,
      dodato23_4pf_group_by.pr_name1,
      ur_fiz_from_proch_2005_group_by.pr_cod_psv,
      ur_fiz_from_proch_2005_group_by.pr_name2,
      "dodatok23_4pf"
FROM
      dodato23_4pf_group_by AS dodato23_4pf_group_by
      LEFT JOIN ur_fiz_from_proch_2005_group_by AS ur_fiz_from_proch_2005_group_by
      ON dodato23_4pf_group_by.pr_name1=ur_fiz_from_proch_2005_group_by.pr_name2
WHERE
      ur_fiz_from_proch_2005_group_by.pr_name2 IS NULL

UNION

SELECT
      dodato23_4pf_group_by.pr_cod_psv,
      dodato23_4pf_group_by.pr_name1,
      ur_fiz_from_proch_2005_group_by.pr_cod_psv,
      ur_fiz_from_proch_2005_group_by.pr_name2,
      "ur_fiz_from_proch_2005"
FROM
      ur_fiz_from_proch_2005_group_by AS ur_fiz_from_proch_2005_group_by
      LEFT JOIN dodato23_4pf_group_by AS dodato23_4pf_group_by
      ON ur_fiz_from_proch_2005_group_by.pr_name2=dodato23_4pf_group_by.pr_name1
WHERE
      dodato23_4pf_group_by.pr_name1 IS NULL
