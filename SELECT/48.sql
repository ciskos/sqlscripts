SELECT CU.*,users.usr_fio
FROM CU AS CU
LEFT JOIN payer_malinv41r AS payer_malinv41r
ON CU.IAN_NUMIDENT=payer_malinv41r.pr_numident
LEFT JOIN users_malinv41r AS users
ON users.usr_extnum=payer_malinv41r.pr_oper_numid
ORDER BY users.usr_fio
