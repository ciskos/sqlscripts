SELECT
      svidet_oldmen.*,
      da.icrtf_dmcl_phone
FROM
      svidet_oldmen AS svidet_oldmen JOIN svidet_spov AS svidet_spov
      ON svidet_oldmen.uind_numident=svidet_spov.id
      LEFT JOIN da AS da
      ON da.icrtf_numident=svidet_oldmen.uind_numident
GROUP BY
      svidet_oldmen.uind_numident
