SELECT
	ur_sum_2009.id,
	ur_sum_2009.pf_code,
	ur_sum_2009.pr_numident,
	COUNT(	ur_sum_2009.pr_numident),
	ur_sum_2009.pr_name1,
	ur_sum_2009.pens_insp,
	SUM(ur_sum_2009.sum1),
	SUM(ur_sum_2009.sum2),
	SUM(ur_sum_2009.sum3),
	SUM(ur_sum_2009.sum4),
	SUM(ur_sum_2009.sum5),
	SUM(ur_sum_2009.sum6),
	SUM(ur_sum_2009.sum7),
	SUM(ur_sum_2009.sum8),
	SUM(ur_sum_2009.sum9),
	SUM(ur_sum_2009.sum10),
	SUM(ur_sum_2009.sum11),
	SUM(ur_sum_2009.sum12),
	SUM(ur_sum_2009.sum13),
	SUM(ur_sum_2009.sum14),
	SUM(ur_sum_2009.sum15),
	SUM(ur_sum_2009.sum16),
	SUM(ur_sum_2009.sum17),
	SUM(ur_sum_2009.sum18),
	SUM(ur_sum_2009.sum19),
	SUM(ur_sum_2009.sum20),
	SUM(ur_sum_2009.sum21),
	SUM(ur_sum_2009.sum22),
	SUM(ur_sum_2009.sum23),
	SUM(ur_sum_2009.sum24),
	SUM(ur_sum_2009.sum25),
	SUM(ur_sum_2009.sum26),
	SUM(ur_sum_2009.sum27),
	SUM(ur_sum_2009.sum28),
	SUM(ur_sum_2009.sum29),
	users_41r.usr_fio
FROM
	ur_sum_2009 AS ur_sum_2009 LEFT JOIN payer_41r AS payer_41r
	ON payer_41r.pr_numident=ur_sum_2009.pr_numident
	LEFT JOIN users_41r AS users_41r
	ON users_41r.usr_extnum=payer_41r.pr_oper_numid
GROUP BY
	ur_sum_2009.pr_numident
ORDER BY
	users_41r.usr_fio
INTO OUTFILE
	'/home/kot/work/table/upload/ur_sum_2009_out' FIELDS TERMINATED BY '|';
