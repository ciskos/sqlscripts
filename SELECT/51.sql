SELECT
	cu_yana.*,
	users_malinv41r.usr_fio
FROM
	cu_yana AS cu_yana
	LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON cu_yana.id=payer_malinv41r.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON payer_malinv41r.pr_oper_numid=users_malinv41r.usr_extnum
