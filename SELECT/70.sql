SELECT
	COUNT(unload_3_10r.uind_numident)
FROM
	unload_3_10r RIGHT JOIN perssheets_10r
	ON unload_3_10r.uind_numident=perssheets_10r.uind_numident
	LEFT JOIN packlabel_10r ON packlabel_10r.lb_id=perssheets_10r.uind_lb
	LEFT JOIN payer_10r ON payer_10r.pr_id=packlabel_10r.lb_pr
