SELECT
      ur_2006_070405.*,
      users.usr_fio
FROM
      ur_2006_070405 AS ur_2006_070405 LEFT JOIN payer AS payer
      ON ur_2006_070405.pr_numident=payer.pr_numident
      LEFT JOIN users AS users ON payer.pr_oper_numid=users.usr_extnum
ORDER BY
      users.usr_fio,
      ur_2006_070405.pr_numident,
      ur_2006_070405.month
