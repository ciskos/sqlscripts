SELECT
	fiz_2010.pf_num,
	fiz_2010.pr_numident,
	fiz_2010.pr_name,
	fiz_2010.doh_insp,
	MAX(fiz_2010.months),
	SUM(fiz_2010.sum_1),
	SUM(fiz_2010.sum_2),
	SUM(fiz_2010.sum_3),
	SUM(fiz_2010.sum_4),
	SUM(fiz_2010.sum_5),
	SUM(fiz_2010.sum_6),
	SUM(fiz_2010.sum_7),
	SUM(fiz_2010.sum_8),
	SUM(fiz_2010.sum_9),
	SUM(fiz_2010.sum_10),
	SUM(fiz_2010.sum_11),
	SUM(fiz_2010.sum_12),
	SUM(fiz_2010.sum_13),
	SUM(fiz_2010.sum_14),
	SUM(fiz_2010.sum_15),
	SUM(fiz_2010.sum_16),
	SUM(fiz_2010.sum_17),
	SUM(fiz_2010.sum_18),
	SUM(fiz_2010.sum_19),
	SUM(fiz_2010.sum_20),
	SUM(fiz_2010.sum_21),
	SUM(fiz_2010.sum_22),
	users_malinv41r.usr_fio
FROM
	fiz_2010 AS fiz_2010 LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=fiz_2010.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	fiz_2010.pr_numident
ORDER BY
	users_malinv41r.usr_fio,
	fiz_2010.pr_numident
INTO OUTFILE '/home/kot/tmp/summ/fiz2010.csv'
