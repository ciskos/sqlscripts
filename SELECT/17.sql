SELECT
      svidet_oldmen.*,
      svidet_spov.*
FROM
      svidet_oldmen AS svidet_oldmen JOIN svidet_spov AS svidet_spov
      ON svidet_oldmen.uind_numident=svidet_spov.id
GROUP BY
      svidet_oldmen.uind_numident
