SELECT
	payer_10r.pr_numident,
	payer_10r.pr_name1,
	payer_10r.pr_name2,
	payer_10r.pr_name3,
	users_ikis.usr_fio
FROM
	payer_10r AS payer_10r LEFT JOIN users_ikis AS users_ikis
	ON users_ikis.usr_extnum=payer_10r.pr_oper_numid
ORDER BY
	users_ikis.usr_fio
INTO OUTFILE '/home/kot/work/table/upload/payers_10r_+_insps' FIELDS TERMINATED BY '|';
