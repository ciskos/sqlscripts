SELECT
	unload_3_41r.uind_numident,
	unload_3_41r.lb_id_in,
	ikis_indani.num_from_pfu,
	unload_3_41r.uip_ln,
	unload_3_41r.uip_nm,
	unload_3_41r.uip_ftn,
	unload_3_41r.pr_numident,
	unload_3_41r.pr_incode,
	unload_3_41r.pr_name1,
	unload_3_41r.uind_charg_year,
	(CASE unload_3_41r.uind_formtype
		WHEN '1' THEN 'Початкова'
		WHEN '2' THEN 'Коригуюча'
		WHEN '3' THEN 'Скасовуюча'
		WHEN '4' THEN 'Призначення пенсии'
	END),
	unload_3_41r.uind_status,
	unload_3_41r.uind_otk,
	unload_3_41r.uind_summp,
	unload_3_41r.uind_summl,
	unload_3_41r.uind_summpc,
	unload_3_41r.uind_summ_pr,
	unload_3_41r.uind_summ_zo,
	users_ikis.usr_fio
FROM
	unload_3_41r AS unload_3_41r LEFT JOIN ikis_indani AS ikis_indani
	ON unload_3_41r.pr_numident=ikis_indani.pr_numident
	AND unload_3_41r.pr_incode=ikis_indani.pr_incode
	AND unload_3_41r.uind_numident=ikis_indani.uind_numident
	AND unload_3_41r.uind_charg_year=ikis_indani.uind_charg_year
	AND unload_3_41r.uind_formtype=ikis_indani.uind_formtype
	AND unload_3_41r.uind_insurcat=ikis_indani.uind_insurcat
	LEFT JOIN ikis2spov_users AS ikis2spov_users
	ON ikis2spov_users.pr_numident=unload_3_41r.pr_numident
	LEFT JOIN users_ikis AS users_ikis
	ON users_ikis.usr_extnum=ikis2spov_users.usr_extnum
WHERE
	ikis_indani.pr_numident IS NULL

ORDER BY
	users_ikis.usr_fio
	INTO OUTFILE '/home/kot/work/table/upload/41r_outload' fields terminated by '|';
