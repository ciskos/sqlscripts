SELECT
	fiz_sum_261109.id,
	fiz_sum_261109.pr_numident,
	fiz_sum_261109.pf_code,
	fiz_sum_261109.pr_name1,
	fiz_sum_261109.pens_insp,
	MAX(fiz_sum_261109.max1),
	MAX(fiz_sum_261109.max2),
	MAX(fiz_sum_261109.max3),
	MAX(fiz_sum_261109.month),
	SUM(fiz_sum_261109.sum1),
	SUM(fiz_sum_261109.sum2),
	SUM(fiz_sum_261109.sum3),
	SUM(fiz_sum_261109.sum4),
	SUM(fiz_sum_261109.sum5),
	SUM(fiz_sum_261109.sum6),
	SUM(fiz_sum_261109.sum7),
	users_41r.usr_fio
FROM
	fiz_sum_261109 AS fiz_sum_261109 LEFT JOIN payer_41r AS payer_41r
	ON payer_41r.pr_numident=fiz_sum_261109.pr_numident
	LEFT JOIN users_41r AS users_41r
	ON users_41r.usr_extnum=payer_41r.pr_oper_numid
GROUP BY
	fiz_sum_261109.pr_numident
ORDER BY
	users_41r.usr_fio,
	fiz_sum_261109.pr_numident
INTO OUTFILE '/home/kot/work/table/upload/fiz_sum_261109'
    FIELDS TERMINATED BY '|';
