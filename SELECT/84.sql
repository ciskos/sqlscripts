SELECT
	ur_sum_261109.id,
	ur_sum_261109.pr_numident,
	ur_sum_261109.pf_code,
	ur_sum_261109.pr_name1,
	ur_sum_261109.pens_insp,
	MAX(ur_sum_261109.max1),
	MAX(ur_sum_261109.max2),
	MAX(ur_sum_261109.max3),
	MAX(ur_sum_261109.month),
	SUM(ur_sum_261109.sum1),
	SUM(ur_sum_261109.sum2),
	SUM(ur_sum_261109.sum3),
	SUM(ur_sum_261109.sum4),
	SUM(ur_sum_261109.sum5),
	SUM(ur_sum_261109.sum6),
	SUM(ur_sum_261109.sum7),
	users_41r.usr_fio
FROM
	ur_sum_261109 AS ur_sum_261109 LEFT JOIN payer_41r AS payer_41r
	ON payer_41r.pr_numident=ur_sum_261109.pr_numident
	LEFT JOIN users_41r AS users_41r
	ON users_41r.usr_extnum=payer_41r.pr_oper_numid
GROUP BY
	ur_sum_261109.pr_numident
ORDER BY
	users_41r.usr_fio,
	ur_sum_261109.pr_numident
INTO OUTFILE '/home/kot/work/table/upload/ur_sum_261109'
    FIELDS TERMINATED BY '|';
