SELECT
      x_files.uind_numident,
      da.icrtf_sht_code,
      da.icrtf_sht_ser,
      da.icrtf_sht_name,
      payer_ank.pr_name2,
      payer_ank.pr_cod_psv,
      payer_ank.pr_ind_post,
      payer_ank.pr_occupitem,
      payer_ank.pr_street,
      payer_ank.pr_building,
      payer_ank.pr_block,
      payer_ank.pr_phone,
      payer_ank.pr_name3,
      da.icrtf_numident,
      da.icrtf_ln,
      da.icrtf_nm,
      da.icrtf_ftn,
      da.icrtf_birthday,
      da.icrtf_sex,
      da.icrtf_birth_countr,
      da.icrtf_birth_area,
      da.icrtf_birth_reg,
      da.icrtf_birth_town,
      da.icrtf_dmcl_pindex,
      da.icrtf_dmcl_koatuu,
      da.icrtf_dmcl_area,
      da.icrtf_dmcl_reg,
      da.icrtf_dmcl_town,
      da.icrtf_dmcl_street,
      da.icrtf_dmcl_build,
      da.icrtf_dmcl_housing,
      da.icrtf_dmcl_flat,
      da.icrtf_dmcl_phone,
      da.icrtf_idoc_name,
      da.icrtf_idoc_series,
      da.icrtf_idoc_dlv_dt,
      da.icrtf_idoc_dlv_pl,
      da.icrtf_idoc_num,
      da.icrtf_form_type,
      users.usr_fio
FROM
      x_files AS x_files LEFT JOIN da AS da
      ON x_files.uind_numident=da.icrtf_numident
      LEFT JOIN payer AS payer
      ON payer.pr_numident=da.icrtf_sht_code
      LEFT JOIN payer_ank AS payer_ank
      ON payer_ank.pr_id=payer.pr_id
      LEFT JOIN users AS users
      ON users.usr_extnum=payer.pr_oper_numid
GROUP BY
      x_files.uind_numident
ORDER BY
      users.usr_fio,
      da.icrtf_ln
