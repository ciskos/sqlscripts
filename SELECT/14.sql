SELECT
      svidet_deadmen.*,
      da.icrtf_numident,
      da.icrtf_ln,
      da.icrtf_nm,
      da.icrtf_ftn,
      svidet_spov.*
FROM
      svidet_spov AS svidet_spov JOIN svidet_deadmen AS svidet_deadmen
      ON svidet_spov.id=svidet_deadmen.uind_numident
      LEFT JOIN da AS da
      ON da.icrtf_numident=svidet_deadmen.uind_numident
GROUP BY
      svidet_deadmen.uind_numident
ORDER BY
      svidet_deadmen.uind_numident
