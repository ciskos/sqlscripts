SELECT
	4pf,
	2007fiz.pr_numident,
	2007fiz.pr_name1,
	MAX(2007fiz.month),
	2007fiz.year,
	MAX(2007fiz.max1),
	MAX(2007fiz.max2),
	SUM(2007fiz.sum1),
	SUM(2007fiz.sum2),
	SUM(2007fiz.sum3),
	SUM(2007fiz.sum4),
	SUM(2007fiz.sum5),
	SUM(2007fiz.sum6),
	SUM(2007fiz.sum7),
	SUM(2007fiz.sum8),
	SUM(2007fiz.sum9),
	SUM(2007fiz.sum10),
	SUM(2007fiz.sum11),
	users_malinv41r.usr_fio
FROM
	2007fiz AS 2007fiz LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=2007fiz.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	2007fiz.pr_numident
ORDER BY
	users_malinv41r.usr_fio,
	2007fiz.pr_numident
