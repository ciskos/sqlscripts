SELECT
      dodatok_25_2006.*,
      users.usr_fio
FROM
      dodatok_25_2006 AS dodatok_25_2006 LEFT JOIN payer AS payer
      ON dodatok_25_2006.pr_numident=payer.pr_numident
      LEFT JOIN users AS users
      ON users.usr_extnum=payer.pr_oper_numid
ORDER BY
      users.usr_fio
