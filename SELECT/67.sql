SELECT
	perssheets_10r.uind_numident,
	indani_pib_10r.uip_ln,
	indani_pib_10r.uip_nm,
	indani_pib_10r.uip_ftn,
	packlabel_10r.lb_id_in
FROM
	perssheets_10r AS perssheets_10r LEFT JOIN packlabel_10r AS packlabel_10r
	ON packlabel_10r.lb_id=perssheets_10r.uind_lb
	LEFT JOIN indani_pib_10r AS indani_pib_10r
	ON indani_pib_10r.uip_uind=perssheets_10r.uind_id
WHERE
	perssheets_10r.uind_numident='1551608176';
