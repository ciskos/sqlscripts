SELECT
	svidet_in_spov.*
FROM
	svidet_2007_obl AS svidet_2007_obl LEFT JOIN svidet_2007_spov AS svidet_2007_spov
	ON svidet_2007_obl.uind_numident=svidet_2007_spov.uind_numident
	RIGHT JOIN svidet_in_spov AS svidet_in_spov
	ON svidet_in_spov.uind_numident=svidet_2007_obl.uind_numident
WHERE svidet_2007_spov.uind_numident IS NULL
ORDER BY
	svidet_2007_obl.uind_numident
