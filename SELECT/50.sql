SELECT cu_1_2.*,users.usr_fio,payer_malinv41r.pr_name1
FROM cu_1_2 AS cu_1_2
LEFT JOIN payer_malinv41r AS payer_malinv41r
ON cu_1_2.IAN_NUMIDENT=payer_malinv41r.pr_numident
LEFT JOIN users_malinv41r AS users
ON users.usr_extnum=payer_malinv41r.pr_oper_numid
ORDER BY users.usr_fio
