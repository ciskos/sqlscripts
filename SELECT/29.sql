SELECT
payer_malinv41r.pr_numident,
payer_malinv41r.pr_name1,
svidet_in_spov_31_12_2006.*,
svidet_2007_spov.*,
indani_pib_malinv41r.uip_ln,
indani_pib_malinv41r.uip_nm,
indani_pib_malinv41r.uip_ftn,
users_malinv41r.usr_fio

FROM
svidet_in_spov_31_12_2006 AS svidet_in_spov_31_12_2006
LEFT JOIN svidet_2007_spov AS svidet_2007_spov
ON svidet_in_spov_31_12_2006.uind_numident = svidet_2007_spov.uind_numident

LEFT JOIN payer_malinv41r AS payer_malinv41r
ON payer_malinv41r.pr_numident=svidet_in_spov_31_12_2006.pr_numident

LEFT JOIN perssheets_malinv41r AS perssheets_malinv41r
ON perssheets_malinv41r.uind_numident=svidet_in_spov_31_12_2006.uind_numident

LEFT JOIN indani_pib_malinv41r AS indani_pib_malinv41r
ON indani_pib_malinv41r.uip_uind=perssheets_malinv41r.uind_id

LEFT JOIN users_malinv41r AS users_malinv41r
ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid

WHERE
svidet_2007_spov.uind_numident IS NOT NULL

GROUP BY
svidet_in_spov_31_12_2006.uind_numident,
svidet_in_spov_31_12_2006.pr_numident,
svidet_in_spov_31_12_2006.kod_rajona

ORDER BY
svidet_2007_spov.uind_numident
