SELECT
payer_illich07r.pr_numident,
payer_illich07r.pr_name1,
svidet_in_spov_31_12_2006.*,
svidet_2007_spov.*,
indani_pib_illich07r.uip_ln,
indani_pib_illich07r.uip_nm,
indani_pib_illich07r.uip_ftn,
users_illich07r.usr_fio

FROM
svidet_in_spov_31_12_2006 AS svidet_in_spov_31_12_2006
LEFT JOIN svidet_2007_spov AS svidet_2007_spov
ON svidet_in_spov_31_12_2006.uind_numident = svidet_2007_spov.uind_numident

LEFT JOIN payer_illich07r AS payer_illich07r
ON payer_illich07r.pr_numident=svidet_in_spov_31_12_2006.pr_numident

LEFT JOIN perssheets_illich07r AS perssheets_illich07r
ON perssheets_illich07r.uind_numident=svidet_in_spov_31_12_2006.uind_numident

LEFT JOIN indani_pib_illich07r AS indani_pib_illich07r
ON indani_pib_illich07r.uip_uind=perssheets_illich07r.uind_id

LEFT JOIN users_illich07r AS users_illich07r
ON users_illich07r.usr_extnum=payer_illich07r.pr_oper_numid

WHERE
svidet_2007_spov.uind_numident IS NOT NULL

GROUP BY
svidet_in_spov_31_12_2006.uind_numident,
svidet_in_spov_31_12_2006.pr_numident,
svidet_in_spov_31_12_2006.kod_rajona

ORDER BY
svidet_2007_spov.uind_numident
