SELECT
      org_ved_new.pr_numident,
      org_ved_new.pr_filial,
      payer.pr_name1,
      org_ved_new.kol_zo,
      svidet_found.rajon,
      svidet_found.pr_numident,
      svidet_found.uind_numident,
      indani_pib.uip_ln,
      indani_pib.uip_nm,
      indani_pib.uip_ftn,
      users.usr_fio
FROM
      perssheets AS perssheets LEFT JOIN indani_pib AS indani_pib
        ON perssheets.uind_id=indani_pib.uip_uind
      LEFT JOIN svidet_found AS svidet_found
        ON perssheets.uind_numident=svidet_found.uind_numident
      LEFT JOIN org_ved_new AS org_ved_new
        ON org_ved_new.pr_numident=svidet_found.pr_numident
      LEFT JOIN payer AS payer
        ON org_ved_new.pr_numident=payer.pr_numident
      LEFT JOIN users AS users
        ON users.usr_extnum=payer.pr_oper_numid
WHERE SELECT
      org_ved_new.pr_numident,
      org_ved_new.pr_filial,
      org_ved_new.pr_name1,
      svidet_found.rajon,
      svidet_found.pr_numident,
      svidet_found.uind_numident
FROM
      org_ved_new AS org_ved_new,
      svidet_found AS svidet_found
WHERE
      org_ved_new.pr_numident=svidet_found.pr_numident
      AND svidet_found.rajon='1541'
ORDER BY
      org_ved_new.pr_numident
LIMIT 0,30
      svidet_found.rajon='1541'
GROUP BY
      org_ved_new.pr_numident,
      perssheets.uind_numident
ORDER BY
      users.usr_fio,
      org_ved_new.pr_numident
LIMIT 0,30
