SELECT
	4pf,
	2007ur.pr_numident,
	2007ur.pr_name1,
	MAX(2007ur.month),
	2007ur.year,
	MAX(2007ur.max1),
	MAX(2007ur.max2),
	SUM(2007ur.sum1),
	SUM(2007ur.sum2),
	SUM(2007ur.sum3),
	SUM(2007ur.sum4),
	SUM(2007ur.sum5),
	SUM(2007ur.sum6),
	SUM(2007ur.sum7),
	SUM(2007ur.sum8),
	SUM(2007ur.sum9),
	SUM(2007ur.sum10),
	SUM(2007ur.sum11),
	users_malinv41r.usr_fio
FROM
	2007ur AS 2007ur LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=2007ur.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	2007ur.pr_numident
ORDER BY
	users_malinv41r.usr_fio,
	2007ur.pr_numident
