SELECT
	COUNT(payer_07r.pr_numident),
	payer_07r.pr_numident,
	payer_07r.pr_name1,
	payer_07r.pr_name2,
	payer_07r.pr_name3,
	perssheets_07r.uind_formtype,
	COUNT(perssheets_07r.uind_formtype),
	users_ikis.usr_fio
FROM
	incomes_07r AS incomes_07r LEFT JOIN perssheets_07r AS perssheets_07r ON perssheets_07r.uind_id=incomes_07r.uind_id
	LEFT JOIN indani_pib_07r AS indani_pib_07r ON indani_pib_07r.uip_uind=perssheets_07r.uind_id
	LEFT JOIN experience_07r_0 AS experience_07r_0 ON experience_07r_0.exp_uind=perssheets_07r.uind_id
	LEFT JOIN experience_07r_1 AS experience_07r_1 ON experience_07r_1.exp_uind=perssheets_07r.uind_id
	LEFT JOIN experience_07r_2 AS experience_07r_2 ON experience_07r_2.exp_uind=perssheets_07r.uind_id
	LEFT JOIN experience_07r_3 AS experience_07r_3 ON experience_07r_3.exp_uind=perssheets_07r.uind_id
	LEFT JOIN packlabel_07r AS packlabel_07r ON packlabel_07r.lb_id=perssheets_07r.uind_lb
	LEFT JOIN payer_07r AS payer_07r ON payer_07r.pr_id=packlabel_07r.lb_pr
	LEFT JOIN ikis2spov_users AS ikis2spov_users ON ikis2spov_users.pr_numident=payer_07r.pr_numident
	LEFT JOIN users_ikis AS users_ikis ON users_ikis.usr_extnum=ikis2spov_users.usr_extnum
GROUP BY
	payer_07r.pr_numident,
	perssheets_07r.uind_formtype
ORDER BY
	users_ikis.usr_fio,
	COUNT(payer_07r.pr_numident),
	perssheets_07r.uind_formtype
INTO OUTFILE '/home/kot/work/table/upload/07r_zo_count_formtype.txt' FIELDS TERMINATED BY '|';
