SELECT
	COUNT(unload_3_41r.pr_numident)
FROM
	unload_3_41r AS unload_3_41r LEFT JOIN ikis_indani AS ikis_indani
	ON unload_3_41r.pr_numident=ikis_indani.pr_numident
	AND unload_3_41r.pr_incode=ikis_indani.pr_incode
	AND unload_3_41r.uind_numident=ikis_indani.uind_numident
	AND unload_3_41r.uind_charg_year=ikis_indani.uind_charg_year
	AND unload_3_41r.uind_formtype=ikis_indani.uind_formtype
	LEFT JOIN ikis2spov_users AS ikis2spov_users
	ON ikis2spov_users.pr_numident=unload_3_41r.pr_numident

WHERE
	ikis_indani.pr_numident IS NULL
