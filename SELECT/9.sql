SELECT
      fiz_2006_ne_null.*,
      users.usr_fio
FROM
      fiz_2006_ne_null AS fiz_2006_ne_null LEFT JOIN payer AS payer
      ON fiz_2006_ne_null.pr_numident=payer.pr_numident
      LEFT JOIN users AS users ON payer.pr_oper_numid=users.usr_extnum
ORDER BY
      users.usr_fio,
      fiz_2006_ne_null.pr_numident,
      fiz_2006_ne_null.month
