SELECT
	payer_10r.pr_numident,
	payer_10r.pr_name1,
	payer_10r.pr_name2,
	payer_10r.pr_name3,
	COUNT(payer_10r.pr_numident)
FROM
	perssheets_10r AS perssheets_10r LEFT JOIN packlabel_10r AS packlabel_10r
	ON packlabel_10r.lb_id=perssheets_10r.uind_lb
	LEFT JOIN payer_10r AS payer_10r
	ON payer_10r.pr_id=packlabel_10r.lb_pr
GROUP BY
	payer_10r.pr_numident
INTO OUTFILE '/home/kot/work/table/upload/payer_10r_workers_count' FIELDS TERMINATED BY '|';
