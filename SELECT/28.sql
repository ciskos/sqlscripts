SELECT
	*
FROM
	svidet_2007_1 AS svidet_2007_1 LEFT JOIN svidet_2007_spov AS svidet_2007_spov
	ON svidet_2007_1.uind_numident=svidet_2007_spov.uind_numident
WHERE svidet_2007_spov.uind_numident IS NULL
ORDER BY
	svidet_2007_1.uind_numident
