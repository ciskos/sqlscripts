SELECT
	svidet_in_spov.*,
	perssheets.uind_numident,
	indani_pib.uip_ln,
	indani_pib.uip_nm,
	indani_pib.uip_ftn
FROM
	perssheets AS perssheets LEFT JOIN indani_pib AS indani_pib
	ON perssheets.uind_id=indani_pib.uip_uind
	LEFT JOIN svidet_2007_obl AS svidet_2007_obl
	ON svidet_2007_obl.uind_numident=perssheets.uind_numident
	LEFT JOIN svidet_2007_spov AS svidet_2007_spov
	ON svidet_2007_obl.uind_numident=svidet_2007_spov.uind_numident
	RIGHT JOIN svidet_in_spov AS svidet_in_spov
	ON svidet_in_spov.uind_numident=svidet_2007_obl.uind_numident
WHERE svidet_2007_spov.uind_numident IS NULL
GROUP BY
	svidet_in_spov.uind_numident,
	svidet_in_spov.kod_rajon,
	svidet_in_spov.pr_numident
ORDER BY
	indani_pib.uip_ln,
	indani_pib.uip_nm,
	indani_pib.uip_ftn,
	svidet_in_spov.uind_numident
