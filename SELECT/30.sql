SELECT
payer_malinv10r.pr_numident,
payer_malinv10r.pr_name1,
svidet_in_spov_31_12_2006.*,
svidet_2007_spov.*,
indani_pib_malinv10r.uip_ln,
indani_pib_malinv10r.uip_nm,
indani_pib_malinv10r.uip_ftn,
users_malinv10r.usr_fio

FROM
svidet_in_spov_31_12_2006 AS svidet_in_spov_31_12_2006
LEFT JOIN svidet_2007_spov AS svidet_2007_spov
ON svidet_in_spov_31_12_2006.uind_numident = svidet_2007_spov.uind_numident

LEFT JOIN payer_malinv10r AS payer_malinv10r
ON payer_malinv10r.pr_numident=svidet_in_spov_31_12_2006.pr_numident

LEFT JOIN perssheets_malinv10r AS perssheets_malinv10r
ON perssheets_malinv10r.uind_numident=svidet_in_spov_31_12_2006.uind_numident

LEFT JOIN indani_pib_malinv10r AS indani_pib_malinv10r
ON indani_pib_malinv10r.uip_uind=perssheets_malinv10r.uind_id

LEFT JOIN users_malinv10r AS users_malinv10r
ON users_malinv10r.usr_extnum=payer_malinv10r.pr_oper_numid

WHERE
svidet_2007_spov.uind_numident IS NOT NULL

GROUP BY
svidet_in_spov_31_12_2006.uind_numident,
svidet_in_spov_31_12_2006.pr_numident,
svidet_in_spov_31_12_2006.kod_rajona

ORDER BY
svidet_2007_spov.uind_numident
