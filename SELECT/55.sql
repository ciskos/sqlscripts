SELECT
	chetire_procenta_stavka_1.*,
	ikis_spov_payers_insps.*,
	users_malinv41r.usr_fio
FROM
	chetire_procenta_stavka_1 AS chetire_procenta_stavka_1 LEFT JOIN
	ikis_spov_payers_insps AS ikis_spov_payers_insps
	ON chetire_procenta_stavka_1.pr_numident=ikis_spov_payers_insps.pr_numident
	LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=ikis_spov_payers_insps.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
ORDER BY
	users_malinv41r.usr_fio
