SELECT
	4pf,
	data240308.pr_numident,
	data240308.pr_name1,
	data240308.pens_insp,
	MAX(data240308.month),
	COUNT(data240308.month),
	SUM(data240308.sum1),
	SUM(data240308.sum2),
	SUM(data240308.sum3),
	SUM(data240308.sum4),
	SUM(data240308.sum5),
	SUM(data240308.sum6),
	SUM(data240308.sum7),
	SUM(data240308.sum8),
	SUM(data240308.sum9),
	SUM(data240308.sum10),
	SUM(data240308.sum11),
	SUM(data240308.sum12),
	SUM(data240308.sum13),
	SUM(data240308.sum14),
	SUM(data240308.sum15),
	SUM(data240308.sum16),
	SUM(data240308.sum17),
	SUM(data240308.sum18),
	SUM(data240308.sum19),
	SUM(data240308.sum20),
	SUM(data240308.sum21),
	SUM(data240308.sum22),
	SUM(data240308.sum23),
	SUM(data240308.sum24),
	users_malinv41r.usr_fio
FROM
	data240308 AS data240308 LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=data240308.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	data240308.pr_numident
ORDER BY
	users_malinv41r.usr_fio,
	data240308.pr_numident
