SELECT
	4pf,
	fiz110208.pr_numident,
	fiz110208.pr_name1,
	fiz110208.pens_insp,
	MAX(fiz110208.month),
	COUNT(fiz110208.month),
	SUM(fiz110208.sum1),
	SUM(fiz110208.sum2),
	SUM(fiz110208.sum3),
	SUM(fiz110208.sum4),
	SUM(fiz110208.sum5),
	SUM(fiz110208.sum6),
	SUM(fiz110208.sum7),
	SUM(fiz110208.sum8),
	SUM(fiz110208.sum9),
	SUM(fiz110208.sum10),
	SUM(fiz110208.sum11),
	SUM(fiz110208.sum12),
	SUM(fiz110208.sum13),
	SUM(fiz110208.sum14),
	SUM(fiz110208.sum15),
	SUM(fiz110208.sum16),
	SUM(fiz110208.sum17),
	SUM(fiz110208.sum18),
	SUM(fiz110208.sum19),
	SUM(fiz110208.sum20),
	SUM(fiz110208.sum21),
	SUM(fiz110208.sum22),
	users_malinv41r.usr_fio
FROM
	fiz110208 AS fiz110208 LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=fiz110208.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	fiz110208.pr_numident
ORDER BY
	users_malinv41r.usr_fio,
	fiz110208.pr_numident
