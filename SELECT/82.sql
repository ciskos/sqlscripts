SELECT
	uind_id,
	i1.uinc_summp,
	i1.uinc_summpc,
	i1.uinc_summl,
	i1.uinc_summv,
	i1.uinc_wrkdays,
	i2.uinc_summp,
	i2.uinc_summpc,
	i2.uinc_summl,
	i2.uinc_summv,
	i2.uinc_wrkdays,
	i3.uinc_summp,
	i3.uinc_summpc,
	i3.uinc_summl,
	i3.uinc_summv,
	i3.uinc_wrkdays,
	i4.uinc_summp,
	i4.uinc_summpc,
	i4.uinc_summl,
	i4.uinc_summv,
	i4.uinc_wrkdays,
	i5.uinc_summp,
	i5.uinc_summpc,
	i5.uinc_summl,
	i5.uinc_summv,
	i5.uinc_wrkdays,
	i6.uinc_summp,
	i6.uinc_summpc,
	i6.uinc_summl,
	i6.uinc_summv,
	i6.uinc_wrkdays,
	i7.uinc_summp,
	i7.uinc_summpc,
	i7.uinc_summl,
	i7.uinc_summv,
	i7.uinc_wrkdays,
	i8.uinc_summp,
	i8.uinc_summpc,
	i8.uinc_summl,
	i8.uinc_summv,
	i8.uinc_wrkdays,
	i9.uinc_summp,
	i9.uinc_summpc,
	i9.uinc_summl,
	i9.uinc_summv,
	i9.uinc_wrkdays,
	i10.uinc_summp,
	i10.uinc_summpc,
	i10.uinc_summl,
	i10.uinc_summv,
	i10.uinc_wrkdays,
	i11.uinc_summp,
	i11.uinc_summpc,
	i11.uinc_summl,
	i11.uinc_summv,
	i11.uinc_wrkdays,
	i12.uinc_summp,
	i12.uinc_summpc,
	i12.uinc_summl,
	i12.uinc_summv,
	i12.uinc_wrkdays
FROM
	perssheets,
	OUTER(incomes i1),
	OUTER(incomes i2),
	OUTER(incomes i3),
	OUTER(incomes i4),
	OUTER(incomes i5),
	OUTER(incomes i6),
	OUTER(incomes i7),
	OUTER(incomes i8),
	OUTER(incomes i9),
	OUTER(incomes i10),
	OUTER(incomes i11),
	OUTER(incomes i12)
WHERE
	i1.uinc_uind=perssheets.uind_id AND i1.uinc_month=1
	AND i2.uinc_uind=perssheets.uind_id AND i2.uinc_month=2
	AND i3.uinc_uind=perssheets.uind_id AND i3.uinc_month=3
	AND i4.uinc_uind=perssheets.uind_id AND i4.uinc_month=4
	AND i5.uinc_uind=perssheets.uind_id AND i5.uinc_month=5
	AND i6.uinc_uind=perssheets.uind_id AND i6.uinc_month=6
	AND i7.uinc_uind=perssheets.uind_id AND i7.uinc_month=7
	AND i8.uinc_uind=perssheets.uind_id AND i8.uinc_month=8
	AND i9.uinc_uind=perssheets.uind_id AND i9.uinc_month=9
	AND i10.uinc_uind=perssheets.uind_id AND i10.uinc_month=10
	AND i11.uinc_uind=perssheets.uind_id AND i11.uinc_month=11
	AND i12.uinc_uind=perssheets.uind_id AND i12.uinc_month= 12
