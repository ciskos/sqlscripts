CREATE TABLE da
(
vers_typ	char(2)
,vers_num	char(3)
,id		char(12)

,sht_pack	char(10)
,sht_num	char(10)
,sht_code	char(10)
,sht_ser	char(2)
,sht_name	char(32)
,sht_cat	char(1)
,sht_pgs	int default 0
,sht_wrk	char(10)
,sht_wnm	char(10)
,sht_date	char(10)
,sht_rnum	int default 0
,sht_rcrg	char(2)
,sht_rcpf	char(2)
,sht_rwcd	char(10)
,sht_rwnm	char(16)
,sht_rdt	char(10)

,numident	char(10)
,ln		char(10)
,nm		char(30)
,ftn		char(30)
,birthday	char(10)
,sex		char(1)
,birth_country	char(20)
,birth_area	char(32)
,birth_reg	char(32)
,birth_town	char(32)
,dmcl_pindex	char(5)
,dmcl_koatuu	char(10)
,dmcl_area	char(30)
,dmcl_reg	char(32)
,dmcl_town	char(32)
,dmcl_street	char(32)
,dmcl_build	char(7)
,dmcl_housing	char(4)
,dmcl_flat	char(4)
,dmcl_phone	char(7)
,idoc_name	char(20)
,idoc_series	char(5)
,idoc_dlv_dt	char(10)
,idoc_dlv_pl	char(50)
,idoc_num	char(20)
,pg_num_in_pk	int default 0
,idoc_is_pass	char(1)
,form_type	char(1)
,fill_dt	char(10)

,pku_ord	int default 0
,pku_pnmb	int default 0
,form_series	varchar(5)
,form_num	varchar(10)
,pku_pdt	char(10)

,flg_in		char(1)
,flg_err	char(1)
,code_err	char(50)
,flg_sndp	char(1)
,flg_prnt	char(1)
,flg_pblc	char(1)
,flg_cntr	char(1)
,flg_snrg	char(1)
,flg_iss	char(1)
,flg_del	char(1)
,flg_wait	char(1)

,f_da		int default 0
,d_da		char(10)
,f_tp		int default 0
,d_tp		char(10)
,f_db		int default 0
,d_db		char(10)
,f_nakl		char(15)
,d_nakl		char(10)
,f_dc		int default 0
,d_dc		char(10)

,r1		char(12)
,r2		char(12)
,r3		char(12)
,r4		char(12)
,r5		char(12)
,KEY da__sht_code__idx (sht_code)
,KEY da__numident__idx (numident)
,KEY da__sht_date__idx (sht_date)
,KEY da__sht_rdt__idx (sht_rdt)
)DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
