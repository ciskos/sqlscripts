create table users_malinv41r
  (
    usr_id serial not null,
    usr_login char(8) not null ,
    usr_sec integer,
    usr_fio char(40),
    usr_status char(1),
    last_user integer not null ,
    last_time char(10) null ,
    usr_extnum char(12),
    KEY usr_extnum_idx (usr_extnum)
  ) DEFAULT CHARACTER SET=koi8r COLLATE=koi8r_general_ci
