CREATE TABLE spisok_db_rd
(
pr_numident char(13),
pr_name varchar(254)
)

SELECT
	spisok_db_rd.*,
	users_malinv41r.usr_fio
FROM
	spisok_db_rd AS spisok_db_rd LEFT join payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=spisok_db_rd.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
ORDER BY
	users_malinv41r.usr_fio
