CREATE TABLE sverka
(
ids char(5),
pr_numident char(13),
pr_name varchar(254),
fop varchar(254),
fss varchar(254),
r_dpau varchar(254),
z_dpau varchar(254),
stan varchar(254),
z_pnp varchar(254),
z_ep varchar(254),
z_nep varchar(254),
z_pdv varchar(254),
sum_fop varchar(254)
)

CREATE TABLE sverka_polu
(
pr_numident char(13),
pr_name varchar(254),
months char(1),
sum_1 decimal(16.2),
sum_2 decimal(16.2),
sum_3 decimal(16.2),
sum_4 decimal(16.2),
sum_5 decimal(16.2),
sum_6 decimal(16.2),
sum_7 decimal(16.2),
sum_8 decimal(16.2),
sum_9 decimal(16.2),
sum_10 decimal(16.2),
sum_11 decimal(16.2),
sum_12 decimal(16.2),
sum_13 decimal(16.2),
sum_14 decimal(16.2),
sum_15 decimal(16.2),
sum_16 decimal(16.2),
sum_17 decimal(16.2),
sum_18 decimal(16.2),
sum_19 decimal(16.2),
sum_20 decimal(16.2),
sum_21 decimal(16.2),
sum_22 decimal(16.2),
sum_23 decimal(16.2)
)

SELECT
	sverka.ids,
	sverka.pr_numident,
	sverka.pr_name,
	SUM(sverka_polu.sum_1),
	COUNT(sverka_polu.months),
	sverka.fss,
	sverka.r_dpau,
	sverka.z_dpau,
	sverka.stan,
	sverka.z_pnp,
	sverka.z_ep,
	sverka.z_nep,
	sverka.z_pdv,
	sverka.sum_fop
FROM
	sverka AS sverka LEFT JOIN sverka_polu AS sverka_polu
	ON sverka_polu.pr_numident=sverka.pr_numident
GROUP BY
	sverka_polu.pr_numident

UNION

SELECT
	sverka.ids,
	sverka.pr_numident,
	sverka.pr_name,
	SUM(sverka_polu.sum_1),
	COUNT(sverka_polu.months),
	sverka.fss,
	sverka.r_dpau,
	sverka.z_dpau,
	sverka.stan,
	sverka.z_pnp,
	sverka.z_ep,
	sverka.z_nep,
	sverka.z_pdv,
	sverka.sum_fop
FROM
	sverka AS sverka LEFT JOIN sverka_polu AS sverka_polu
	ON sverka_polu.pr_numident<>sverka.pr_numident
GROUP BY
	sverka_polu.pr_numident
limit 0,10
