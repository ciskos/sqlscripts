create table d_sp
(
pr_numident char(13),
filial varchar(3),
pr_name varchar(254),
uind_numident char(10),
uip_ln varchar(254),
uip_nm varchar(254),
uip_fnm varchar(254)

)

create index uind_numident_index
on d_sp (uind_numident)

SELECT
	d_sp.*,
	users_malinv41r.usr_fio
FROM
	d_sp AS d_sp LEFT JOIN svs_dbal AS svs_dbal
	ON d_sp.uind_numident<>svs_dbal.uind_numident
	LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON d_sp.pr_numident=payer_malinv41r.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
WHERE
	svs_dbal.uind_numident IS NULL
ORDER BY
	users_malinv41r.usr_fio,
	d_sp.pr_numident,
	d_sp.uind_numident
;

SELECT
	d_sp.*
FROM
	d_sp AS d_sp LEFT JOIN svs_dbal AS svs_dbal
	ON d_sp.uind_numident<>svs_dbal.uind_numident
	LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON d_sp.pr_numident=payer_malinv41r.pr_numident
WHERE
	svs_dbal.uind_numident IS NULL
;

SELECT
	d_sp.*
FROM
	d_sp AS d_sp LEFT JOIN svs_dbal AS svs_dbal
	ON d_sp.uind_numident=svs_dbal.uind_numident
WHERE
	svs_dbal.uind_numident IS NULL
;

SELECT
	d_sp.*,
	users_malinv41r.usr_fio
FROM
	d_sp AS d_sp LEFT JOIN svs_db AS svs_db
	ON d_sp.uind_numident=svs_db.tin
	LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON svs_db.krpou=payer_malinv41r.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
WHERE
	svs_db.tin IS NULL
;

SELECT
	d_sp.*,
	users_malinv41r.usr_fio
FROM
	d_sp AS d_sp LEFT JOIN svs_db AS svs_db
	ON d_sp.uind_numident=svs_db.tin
	LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON d_sp.pr_numident=payer_malinv41r.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid

WHERE
	svs_db.tin IS NULL
	AND d_sp.tk='? ??????????????|'
ORDER BY
	users_malinv41r.usr_fio,
	d_sp.pr_numident,
	d_sp.uind_numident
;
SELECT
	*
FROM
	d_sp
WHERE
	tk='?? ? ??????????????|'

where
uind_numident not in (select tin from svs_sb)


CREATE TABLE minimal_zp2
(
pr_numident char(13),
filial varchar(3),
pr_name varchar(254),
zo_num varchar(10),
uind_numident char(10),
uip varchar(254),
sum_1 int(3)
)

CREATE INDEX pr_numident_index
ON minimal_zp2 (pr_numident)

LOAD DATA INFILE '/home/kot/tmp/SPOVA_u' INTO TABLE minimal_zp2
FIELDS TERMINATED BY '|'

SELECT
	minimal_zp2.pr_numident,
	minimal_zp2.filial,
	minimal_zp2.pr_name,
	COUNT(minimal_zp2.zo_num),
	SUM(minimal_zp2.sum_1),
	users_malinv41r.usr_fio
FROM
	minimal_zp2 AS minimal_zp2 LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON payer_malinv41r.pr_numident=minimal_zp2.pr_numident
	LEFT JOIN users_malinv41r AS users_malinv41r
	ON users_malinv41r.usr_extnum=payer_malinv41r.pr_oper_numid
GROUP BY
	minimal_zp2.pr_numident
ORDER BY
	users_malinv41r.usr_fio

SELECT
	minimal_zp2.pr_numident,
	minimal_zp2.filial,
	minimal_zp2.pr_name,
	COUNT(minimal_zp2.zo_num),
	SUM(minimal_zp2.sum_1)
FROM
	minimal_zp2.minimal_zp
GROUP BY
	minimal_zp2.pr_numident
WHERE
	SUM(minimal_zp2.sum_1)<>0


SELECT
	payer_malinv41r.pr_numident,
	perssheets_malinv41r.uind_numident
FROM
	perssheets_malinv41r AS perssheets_malinv41r
	LEFT JOIN packlabel_malinv41r AS packlabel_malinv41r
	ON perssheets_malinv41r.uind_lb=packlabel_malinv41r.lb_id
	LEFT JOIN payer_malinv41r AS payer_malinv41r
	ON packlabel_malinv41r.lb_pr=payer_malinv41r.pr_numident
