TRUNCATE TABLE experience_10r;
TRUNCATE TABLE incomes_10r;
TRUNCATE TABLE indani_pib_10r;
TRUNCATE TABLE npstagdet_10r;
TRUNCATE TABLE npstagdet_pib_10r;
TRUNCATE TABLE packlabel_10r;
TRUNCATE TABLE payer_10r;
TRUNCATE TABLE perssheets_10r;

DROP TABLE experience_10r;
DROP TABLE incomes_10r;
DROP TABLE indani_pib_10r;
DROP TABLE npstagdet_10r;
DROP TABLE npstagdet_pib_10r;
DROP TABLE packlabel_10r;
DROP TABLE payer_10r;
DROP TABLE perssheets_10r;
