UNLOAD TO /u/file_korr2.txt
SELECT
	pr_numident,
	uind_numident,
	uind_summ_pr,
	uind_summ_zo,
	uind_summp,
	uind_summl,
	uind_summpc,
	uind_wrkdays,
	uind_summv,
	uinc_month,
	uinc_summp,
	uind_summl,
	uind_summpc,
	uinc_wrkdays,
	uinc_summv
FROM
	incomes,
	perssheets,
	packlabel,
	payer
WHERE
	uinc_uind=uind_id
	and uind_lb=lb_id
	and lb_pr=pr_id
	and pr_numident='05769299'
	and uind_numident='1330711253'
	and YEAR(uind_charg_year)=2005
