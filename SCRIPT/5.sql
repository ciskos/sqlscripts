CREATE TABLE schet
(
dat char(8 ),
pr_numident char(13),
uind_numident char(10),
sum1 decimal(16,2),
sum2 decimal(16,2),
sum3 decimal(16,2)
)

SELECT
	dat,
	pr_numident,
	uind_numident,
	sum1,
	sum2,
	SUM(sum3)
FROM
	schet
GROUP BY
	uind_numident
