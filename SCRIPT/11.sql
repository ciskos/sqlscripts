DROP INDEX svs_db__krpou__idx ON SVS_DB;
DROP INDEX svs_db__ktf__idx ON SVS_DB;
DROP INDEX svs_db__fulln_u__idx ON SVS_DB;
DROP INDEX svs_db__name_u__idx ON SVS_DB;
DROP INDEX svs_db__fath_u__idx ON SVS_DB;
DROP INDEX svs_db__ser_sv__idx ON SVS_DB;
DROP INDEX svs_db__num_sv__idx ON SVS_DB;
DROP INDEX svs_db__tin__idx ON SVS_DB;


CREATE INDEX svs_db__krpou__idx ON SVS_DB(krpou);
CREATE INDEX svs_db__ktf__idx ON SVS_DB(ktf);
CREATE INDEX svs_db__fulln_u__idx ON SVS_DB(fulln_u);
CREATE INDEX svs_db__name_u__idx ON SVS_DB(name_u);
CREATE INDEX svs_db__fath_u__idx ON SVS_DB(fath_u);
CREATE INDEX svs_db__ser_sv__idx ON SVS_DB(ser_sv);
CREATE INDEX svs_db__num_sv__idx ON SVS_DB(num_sv);
CREATE INDEX svs_db__tin__idx ON SVS_DB(tin);
