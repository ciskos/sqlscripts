CREATE TABLE experience_10r
(
	exp_uind decimal(14,0) not null,
	exp_periodnum smallint not null,
	exp_kplg char(8),
	exp_monthend char(10),
	exp_monthqnt char(10),
	exp_dayqnt char(10),
	exp_hourqnt char(10),
	exp_minitqnt char(10),
	exp_wrkdays char(10),
	exp_normchng char(10),
	exp_norm char(7),
	exp_sison char(10),
	KEY exp_uind_idx (exp_uind)
);

CREATE TABLE incomes_10r
(
	uind_id decimal(14,0) NOT NULL,
	january_summp decimal(16,2),
	january_summpc decimal(16,2),
	january_summl decimal(16,2),
	january_summv decimal(16,2),
	january_wrkdays smallint,
	february_summp decimal(16,2),
	february_summpc decimal(16,2),
	february_summl decimal(16,2),
	february_summv decimal(16,2),
	february_wrkdays smallint,
	march_summp decimal(16,2),
	march_summpc decimal(16,2),
	march_summl decimal(16,2),
	march_summv decimal(16,2),
	march_wrkdays smallint,
	april_summp decimal(16,2),
	april_summpc decimal(16,2),
	april_summl decimal(16,2),
	april_summv decimal(16,2),
	april_wrkdays smallint,
	may_summp decimal(16,2),
	may_summpc decimal(16,2),
	may_summl decimal(16,2),
	may_summv decimal(16,2),
	may_wrkdays smallint,
	june_summp decimal(16,2),
	june_summpc decimal(16,2),
	june_summl decimal(16,2),
	june_summv decimal(16,2),
	june_wrkdays smallint,
	july_summp decimal(16,2),
	july_summpc decimal(16,2),
	july_summl decimal(16,2),
	july_summv decimal(16,2),
	july_wrkdays smallint,
	august_summp decimal(16,2),
	august_summpc decimal(16,2),
	august_summl decimal(16,2),
	august_summv decimal(16,2),
	august_wrkdays smallint,
	september_summp decimal(16,2),
	september_summpc decimal(16,2),
	september_summl decimal(16,2),
	september_summv decimal(16,2),
	september_wrkdays smallint,
	october_summp decimal(16,2),
	october_summpc decimal(16,2),
	october_summl decimal(16,2),
	october_summv decimal(16,2),
	october_wrkdays smallint,
	november_summp decimal(16,2),
	november_summpc decimal(16,2),
	november_summl decimal(16,2),
	november_summv decimal(16,2),
	november_wrkdays smallint,
	december_summp decimal(16,2),
	december_summpc decimal(16,2),
	december_summl decimal(16,2),
	december_summv decimal(16,2),
	december_wrkdays smallint,
	KEY uind_id_idx (uind_id)
);

CREATE TABLE indani_pib_07r
(
	uip_uind decimal(14,0) NOT NULL,
	uip_ln varchar(40) default NULL,
	uip_nm varchar(40) default NULL,
	uip_ftn varchar(40) default NULL,
	KEY uip_uind_idx (uip_uind)
);

CREATE TABLE npstagdet_07r
(
	npst_id decimal(14,0) not null,
	npst_lb decimal(14,0),
	npst_numident char(10),
	npst_privcode char(8),
	npst_monthqnt smallint,
	npst_dayqnt smallint,
	npst_formtype char(1),
	npst_charg_year char(4),
	npst_wrkr decimal(14,0),
	lb_date_pr char(10),
	KEY npst_id_idx (npst_id),
	KEY npst_lb_idx (npst_lb),
	KEY npst_numident_idx (npst_numident)
);

CREATE TABLE npstagdet_pib_07r
(
	npp_npst decimal(14,0) not null,
	npp_ln varchar(40),
	npp_nm varchar(40),
	npp_ftn varchar(40),
	KEY npp_npst_idx (npp_npst)
);

CREATE TABLE packlabel_07r
(
	lb_id decimal(14,0) not null,
	lb_id_in integer,
	lb_org smallint not null,
	lb_num_pr varchar(20),
	lb_date_pr char(10),
	lb_date_accept char(10),
	lb_docktype char(1) not null,
	lb_pr decimal(14,0) not null,
	lb_respons_numid char(10),
	lbt_comment varchar(200),
	usr_fio varchar(255),
	KEY lb_id_idx (lb_id),
	KEY lb_pr_idx (lb_pr)
);

CREATE TABLE payer_07r
(
	pr_id decimal(14,0) not null,
	pr_status char(1) default 'A',
	pr_org smallint not null,
	pr_payercat smallint not null ,
	pr_entrprv smallint,
	pr_numident char(13),
	pr_incode char(5),
	pr_cod_psv varchar(15),
	pr_name1 varchar(254),
	pr_name2 varchar(254),
	pr_name3 varchar(254),
	pr_kved smallint,
	pr_registr_date char(10),
	pr_date_start char(10),
	pr_date_stop char(10),
	pr_ind_post char(6),
	pr_occupitem varchar(60),
	pr_street varchar(60),
	pr_building varchar(20),
	pr_block varchar(20),
	pr_phone varchar(20),
	pr_fax varchar(20),
	pr_internet varchar(30),
	pr_wrkpl_qnt integer,
	pr_wrk_qnt integer,
	pr_invalid_qnt integer,
	pr_oper_numid char(10),
	KEY pr_id_idx (pr_id),
	KEY pr_numident_idx (pr_numident),
	KEY pr_oper_numid_idx (pr_oper_numid)
);

CREATE TABLE perssheets_07r
(
	uind_id decimal(14,0) not null,
	uind_lb decimal(14,0) not null,
	uind_uind_chng decimal(14,0),
	uind_formtype char(1),
	uind_numident char(10),
	uind_insurcat char(2),
	uind_charg_year char(4),
	uind_pay_year char(4),
	uind_year_period char(2),
	uind_otk char(1),
	uind_summ_pr decimal(16,2) default 0.00,
	uind_summ_zo decimal(16,2) default 0.00,
	uind_summp decimal(16,2) default 0.00,
	uind_summpc decimal(16,2) default 0.00,
	uind_summl decimal(16,2) default 0.00,
	uind_summv decimal(16,2) default 0.00,
	uind_wrkdays smallint default 0,
	uind_recept_date char(10),
	uind_dismiss_date char(10),
	uind_fmonth smallint default 0,
	uind_fday smallint default 0,
	uind_date_doc char(10),
	uind_wrkr decimal(14,0),
	KEY uind_id_index (uind_id),
	KEY uind_lb_index (uind_lb),
	KEY uind_numident_index (uind_numident)
);

LOAD DATA INFILE '/home/kot/work/table/download/malinv10r_fd/to_use/experience' INTO TABLE experience_10r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/malinv10r_fd/to_use/incomes' INTO TABLE incomes_10r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/malinv10r_fd/to_use/indani_pib' INTO TABLE indani_pib_10r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/malinv10r_fd/to_use/npstagdet' INTO TABLE npstagdet_10r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/malinv10r_fd/to_use/npstagdet_pib' INTO TABLE npstagdet_pib_10r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/malinv10r_fd/to_use/packlabel' INTO TABLE packlabel_10r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/malinv10r_fd/to_use/payer' INTO TABLE payer_10r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/malinv10r_fd/to_use/perssheets' INTO TABLE perssheets_10r FIELDS TERMINATED BY '|';

LOAD DATA INFILE '/home/kot/work/table/download/illich07r_fd/experience' INTO TABLE experience_07r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/illich07r_fd/incomes' INTO TABLE incomes_07r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/illich07r_fd/indani_pib' INTO TABLE indani_pib_07r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/illich07r_fd/npstagdet' INTO TABLE npstagdet_07r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/illich07r_fd/npstagdet_pib' INTO TABLE npstagdet_pib_07r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/illich07r_fd/packlabel' INTO TABLE packlabel_07r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/illich07r_fd/payer' INTO TABLE payer_07r FIELDS TERMINATED BY '|';
LOAD DATA INFILE '/home/kot/work/table/download/illich07r_fd/perssheets' INTO TABLE perssheets_07r FIELDS TERMINATED BY '|';
